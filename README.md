Dissertation
========

This repository is a dissertation by Tomoki Sugiura.

[![pipeline status](http://bootes.ics.es.osaka-u.ac.jp/s-tomoki/dissertation/badges/master/pipeline.svg)](http://bootes.ics.es.osaka-u.ac.jp/s-tomoki/dissertation/commits/master)

Important Date
-----
* Jan. 9: Submission Deadline by IST office
* Feb. 1: Final Diffence Exam.
* Mar. 22: Graduation Ceremony  

Stable Version
-----

`git clone --depth=1 -b <tag_name> git@bootes.ics.es.osaka-u.ac.jp:s-tomoki/dissertation.git`

* Ver4.1 (Final, 2018 Jan. 9): `ver4.1_final`
* Ver3.0 (2017 Dec. 27): `ver3.0_revisioned`
* Ver2.0 (2017 Dec. 6): `ver2.0_public-hearing`
* Ver1.0 (2017 Dec. 3): `ver1.0`
* 1st draft (2017 Nov. 17): `1st-draft`

To Do List 
-----
Update in Jan. 9th

* Introduction 
* Related work
* Phase Sync.
* Compression 
* Stim. Controller
* Conclusion & F.W.

File Trees
-----

    |-- README.md: This file
    |-- doc : Documents about dissertation
    |-- manuscript : Source of dissertation
    `-- slide : Slides for public-hearing defense

How to compile
-----

    make all

Then, let's view `manuscript/build/main.pdf`

Referees
-----
| | 名前| Name | Address |
|---|---|---|--|
| main | 橋本 昌宜 |  Masanori HASHIMOTO | hasimoto@ist.osaka-u.ac.jp |
| sub | 尾上 孝雄 | Takao ONOYE | onoye@ist.osaka-u.ac.jp | 
| sub | 今井 正治 | Masaharu IMAI | imai@ist.osaka-u.ac.jp | 
| sub | 八木 哲也 | Tetsuya YAGI | yagi@eei.eng.osaka-u.ac.jp |
|---|---|---|
| Hashimoto Sec. | 村上 麻子 | Asako MURAKAMI | murakami.asako@ist.osaka-u.ac.jp | 
| Onoye Sec. | 吉田 友紀 | Yuki YOSHIDA |yoshida.yuki@ist.osaka-u.ac.jp | 
| Yagi Sec. | 中谷 文乃 | Fumino NAKATANI | nakatani@eei.eng.osaka-u.ac.jp |  

Useful reference
-----
* RIS2BIB: https://www.bruot.org/ris2bib/ 
* doi2bib: https://www.doi2bib.org/


Proverb (箴言・金言)
-----

> 序章の目的は，その話題について読者が過去の論文を自分で読まなくても読者がその内容を理解でき，研究結果を正当に評価できるように，背景の情報を十分に与えることです．
> また，序章ではその研究を行うべき論理的な根拠を示さねばなりません．

-- pp.62-63, 10章 序論の書き方, 「暗黙の原則」より


> 学位論文は重要な参考文献を含めるべきです．それによって，あなたがその学問分野について精通していることを示すことができます．
> （中略）
> さらにあなたのテーマについての歴史をひもとくところまで遡って，議論するのが望ましいでしょう．そうすることであなたは自分の学問分野の貴重な文献レビューを集めることができ，それと同時に科学史について何がしか学ぶことができます．そして，科学史というのは教育の上でもっとも大事なものであることがわかるでしょう．

-- p.250, 35章 学術論文の書き方, 「書き方へのアドバイス」より

> あなたの結果と他の人の結果を混在させてはいけません．もしあなたが他の人の結果を示し，あなたの結果を確かめたり，比較したりする場合は，討論(Discussion)の節で行うべきです．

-- p.250, 35章 学術論文の書き方, 「書き方へのアドバイス」より

> 学位論文を書くときには，序論(Introduction)の部分に対して特別の注意を払って書くことをお勧めします．それには2つの理由があります．まず第一に，序論をしっかり書くことは，自分にとって大変ためになります．序論を書くことは，自分がどんな問題に取り組んだか，そしてどのように，またなぜその問題を取り上げたか，それをどうやって解決したか，さらに研究の過程で何を学んだかというようなことを，明らかにすることになるからです．
> 論文の残りの部分は，序論から素直に，論理の流れに従って導かれるものなのです．
> 第二に，第一印象が大切だということがあります．最初に読者を混乱させて，読む意欲を失わせることは得策ではありません．
> そういう意味でも序論は大事なのです．

-- p.251, 35章 学術論文の書き方, 「書き方へのアドバイス」より

