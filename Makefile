
.PHONY: clean gen_manuscript gen_handout pub-hear


all: gen_manuscript gen_handout # pub-hear

gen_manuscript: 
	cd manuscript && make all -j

gen_handout: 
	mkdir -p doc/handout
	cd manuscript && make handout -j && cp build/handout.pdf ../doc/handout/

# pub-hear: 
# 	cd manuscript/pub-hear && make all -j
# I'd like exclude this because CentOS7's pandoc is too old version.
clean: 
	cd manuscript && make clean 
	cd manuscript/pub-hear && make clean
