\renewcommand{\dirpath}{body/chap3}

\chapter{High Throughput Architecture for Phase Synchronization Analysis}

This thesis discusses an architecture of neural signal recording and processing, which has a key role in the precision of closed-loop stimulation.
The neural recorders are divided into two parts: Analog front end and digital signal processor, and a well-known feature extraction method in the digital signal processor is focused on, which is phase synchrony analysis.
For precision and safety of stimulation, the closed-loop control requires a simultaneous pursuit of high-speed performance and low energy consumption for the digital signal processor.
Therefore, this chapter discusses how to improve the throughput of the phase synchrony analyzer and how to decrease hardware implementation cost.
% First, this chapter explains requirements of implantable neural signal processing concerning energy consumption.
In this chapter, a hardware-oriented phase synchrony analysis algorithm is demonstrated, which has lower computational complexity and enables faster processing than a straight-forward conventional method.
A hardware implementation which enables high throughput for feature extraction is also proposed, and then the proposed implementation is compared with one of the conventional methods regarding hardware efficiency.

\section{Motivation and Objective}
\new{
\Acf{eeg} and \acf{ecog} are electrical signals that represent neural activities in the superficial layers of the cortex.
\ac{eeg} is recorded from the scalp, and \ac{ecog} is recorded by subdural grid electrodes on the cortical surface.
 The recent advances in microelectrode technology to acquire electrical signals in biomedical application enables the analysis of \ac{eeg} and \ac{ecog} to not only understand the relationship between neural activity and cognitive behavior but also reveal the pathomechanisms in brain diseases such as seizure of epilepsy and Alzheimer's disease~\cite{Uhlhaas2006, Uhlhaas2010,Uhlhaas2012}.
}

Owe to the outstanding progress of neuroscience, especially prediction with \ac{eeg} and \ac{ecog}~\cite{varela2001brainweb, osorio2011phase,takaura2016frequency, XHu2010PhaseSyncLFP, Williamson2012}, wearable and implantable devices have been able to diagnose numerous disease.
 Low energy consumption design of these devices provides the improvement of ease for carrying and surgery for implantation while excess energy consumption impedes portability.
In these devices, the trade-off between processing speed and energy consumption is always the biggest issue.
For practical applications, many researchers are conducting studies to find out how to realize both computational efficiency and low energy consumption in embedded systems, and hardware acceleration is indispensable for this purpose.
However, most of the algorithms used in these applications are not initially designed for hardware implementation.
Therefore, it is often difficult to break existing trade-off between the execution time and the energy consumption in hardware accelerator design.

In this thesis, we mainly focus on one of phase synchronization analyses, which is \acf{mpc}.
\ac{mpc} is a method proposed by Mormann~\cite{morman2000MPC} for analyzing coordinated activation of distributed brain regions and enables to detect and predict seizures of epilepsy~\cite{Williamson2012}.
The derivation of \ac{mpc} mainly consists of three processing: Band-pass filtering, Hilbert transform, and \ac{mpc} calculation.
The band-pass filtering extracts desired frequency band of interest, the Hilbert transform computes instantaneous phase of each wave, and the \ac{mpc} metric quantifies phase-locking based on the difference between phase time courses of two waves.
The filtering and the Hilbert transform can be efficiently implemented with \ac{fir} filters, but calculating \ac{mpc} metric is computationally expensive in both software and hardware due to the use of trigonometric functions.
Computers cannot calculate identical values of trigonometric functions like sinusoid because the width of registers is limited and finite combinations of four arithmetic operations cannot represent these functions. 
Therefore, calculation of the trigonometric functions uses approximate values that are calculated with approximate equations like Taylor expansion or are stored in look-up tables.
To calculate \ac{mpc} with trigonometric functions, Abdelhalim \etal~\cite{Abdelhalim2010PhaseSync} proposed the implementation that contains \ac{cordic}, which Volder presented in 1959 in work~\cite{Volder1959CORDIC}, but the \ac{cordic} requires a significant number of execution cycles and impedes processing throughput.
Romaine \etal~\cite{Romaine2015DAA,JBRomaine2016PRIME} proposed \ac{dda} which can approximate phase synchronization values without trigonometric functions.
However, the \ac{dda} sacrifices calculation accuracy of the phase synchronization.
Also, Sylmarie \etal~\cite{Sylmarie2017PPDDecimation} proposed a simplified calculation of phase synchrony for \ac{ecog} to decimalize multi-bit input signals to binarize waveform with a statistically determined threshold.
This method enables to reduce the complexity of calculator logics, but it has some drawbacks: This method sacrifices calculation accuracy and its threshold is predetermined value while that depends on characteristics of input signals and changed frequently.

For the increase in \ac{mpc} calculation efficiency, this thesis proposes a novel \ac{mpc} calculation method and its hardware implementation, in which its calculation result is identical to that of the original \ac{mpc}.
The processing target of the proposed method is electrical signals such as \ac{ecog}.
Also, the proposed method is hardware-oriented and realizes the \ac{mpc} calculation without trigonometric functions, so that it can accelerate its process.
This contribution will provide higher accuracy of diagnosis and prediction of the critical situation in neural diseases with the clinical closed-loop stimulator such as \acf{dbs}.



\section{Calculation of Mean Phase Coherence}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.9\textwidth]{\dirpath/fig/obj/overview.pdf}
  \caption{Overview of phase synchronization analyzer.}
  \label{fig:overview}
\end{figure}

This section explains the definition of the \ac{mpc} and the proposed \ac{mpc} calculation method without trigonometric functions.
The phase synchronization analysis can be conducted by either of the following two methods: using the analytic signal or using wavelet transform~\cite{lachaux1999measuring}.
\new{
  The difference between the two methods is minor~\cite{LeVanQuyen2001}, and the analytic signal approach requires less calculation amount than the wavelet approach.
  Therefore, this thesis studies the analytic signal approach.
}

\begin{figure}[tb]
  \centering
  \includegraphics[width=.75\textwidth]{\dirpath/fig/svg/mpc_def}
  \caption{Definition of \acs{mpc}.}
  \label{fig:c3_def-mpc}
\end{figure}

\headline{Mean Phase Coherence}
With the analytic signal approach, the \ac{mpc} indicates the degree of the phase synchronization between the two channels represented by the circular variance of difference signal phase.
\figref{fig:overview} shows the target of phase synchronization analyzer.
\new{
The analyzer obtains biomedical signals $v_k$, like \ac{eeg} and \ac{ecog}, from multichannel electrodes, and then a digital block gets digitalized signal $x_k$ from an analog front end.
Through the \ac{fir} filters, an \ac{mpc} calculator computes each pair of the signals.
}
This study focuses on the \ac{mpc} calculator in the bottom of \figref{fig:overview}.
From \cite{rosenblum1998analysing}, the analytic signal $Z_x(t)$ of a target signal $x(t)$ is given by the following equation: 
\begin{equation}
  Z_x(t) = x(t) + jy(t) = A_x(t)e^{j\phi_x(t)}, \label{eq:hilbert}
\end{equation}
where the function ${y}(t)$ is the Hilbert transform of $x(t)$, and the instantaneous amplitude $A_x(t)$ and the instantaneous phase $\phi_x(t)$ of the signal $x(t)$ are thus uniquely defined from (\ref{eq:hilbert}).
Also, $\phi_x(t)$ in $0 \le \phi < 2\pi$ is defined by the following equation: 
\begin{equation}
  \phi_x(t) = \arctan \left( \frac{y(t)}{x(t)}\right).
\end{equation}
From \cite{morman2000MPC}, the \ac{mpc} of an angular distribution, which is denoted as $R$ in the literature, is defined as:
\begin{equation}
  \mathrm{MPC} = \left| \frac{1}{N} \sum_{i=0}^{N-1} e^{j[\phi_1(t_i)-\phi_0(t_i)]} \right|, \label{eq:def_mpc}
\end{equation}
where $N$ is the number of samples in a window.
The use of Euler's formula turns the above formula into 
\begin{equation}
  \mathrm{MPC} = \left[ \left( \frac{1}{N}\sum_{i=0}^{N-1}\sin[\varphi(i\Delta t)] \right)^2 + \left( \frac{1}{N}\sum_{i=0}^{N-1}\cos[\varphi(i\Delta t)] \right)^2 \right]^{\frac{1}{2}}. \label{eq:eul_mpc}
\end{equation}
An \ac{mpc} calculation method based on (\ref{eq:eul_mpc}) is referred as a conventional method in the following.
From the trigonometric addition theorem, $\varphi(t)$ is calculated by 
\begin{equation}
  \varphi(t) = \phi_1(t) - \phi_0(t) = \arctan \left( \frac{y_0(t) x_1(t) - x_0(t)y_1(t)}{x_0(t)x_1(t) + y_0(t)y_1(t)} \right).
\end{equation}
\new{
  \figureref{fig:c3_def-mpc} indicates the relationship between each vector of input signals and \ac{mpc} value.
(\ref{eq:eul_mpc}) calculates the average of real values and imaginary values of the input signals separately.
As shown in \figref{fig:c3_def-mpc}, \ac{mpc} represents length of the vector, or Euclid norm, whose coordinate consists of the average of the real and imaginary value of the input signals.
}

% ここで三角関数および除算の実装について触れる
\new{
Generally speaking, calculation of the trigonometric functions and division needs a large amount of computation in hardware implementation, and then it results in a significant amount of the execution cycles and the circuit area.
In (\ref{eq:eul_mpc}), sine, cosine, and arctangent are used, and the number of execution times of each function is equal to the number of samples.
The trigonometric functions are calculated with look-up tables, polynomial approximation, or \ac{cordic}.
Calculation using look-up table is efficient when the targetted input range is limited.
However, the input range of transformed signals by Hilbert transform is not limited, and hence the desired look-up table will significantly occupy a considerable amount of the circuit area.
Polynomial approximation like Taylor expansion can calculate trigonometric functions with four basic arithmetic operations.
For example, the Maclaurin expansion of arctangent of $x$ in $-1 < x < 1$, sine of $\theta$, and cosine of $\theta$ are represented by the following equation respectively: 
\begin{align}
  \arctan(x) &= \sum_{n=0}^{\infty} \frac{(-1)^n}{2n+1}x^{2n+1}  \\
  &= x - \frac{x^3}{3} + \frac{x^5}{5} - \frac{x^7}{7} + \cdots,
\end{align}
\begin{align}
  \sin(\theta) &= \sum_{n=0}^{\infty} \frac{(-1)^n}{(2n+1)!}x^{2n+1}  \\
  &= x - \frac{x^3}{3!} + \frac{x^5}{5!} - \frac{x^7}{7!} + \cdots,
\end{align}
\begin{align}
  \cos(\theta) &= \sum_{n=0}^{\infty} \frac{(-1)^n}{2n}x^{2n}  \\
  &= 1 - \frac{x^2}{2!} + \frac{x^4}{4!} - \frac{x^6}{6!} + \cdots.
\end{align}
Note that \ac{mpc} calculation includes sine, cosine, and arctangent function and each approximate calculation is consists of the considerable number of additions and multiplications.
Sine and cosine calculation have to wait to finish the processing of arctangent. 
While the calculation times of division can make zero by substituting for multiplications of constant values, such a large amount of calculation amount impedes throughput of \ac{mpc} implementation.
As proposed in~\cite{Abdelhalim2010PhaseSync}, \ac{cordic} enables to perform trigonometric calculations with adders, shifters, and a look-up table.
On the other hand, the convergence of \ac{cordic} operation for $k$-bit value requires $k$ time iteration, hence \ac{cordic} consumes $k$ execution cycles to calculate the $k$-bit value and this occurs a bottleneck of throughput in \ac{mpc} calculation.
Also, straightforward $k$-bit division consumes $k$ cycles and hardware-acceleration of division requires a large number of gates.
To avoid these high-cost implementations, this thesis provides an \ac{mpc} calculation method without the trigonometric functions and division from (\ref{eq:eul_mpc}) to reduce hardware implementation cost.
}

\headline{Proposed Method}

\begin{figure}[tb]
  \centering
  \includegraphics[width=.85\textwidth]{\dirpath/fig/svg/mpc_eq-expand_vec}
  \caption{Conversion of a vector in (\ref{eq:f_exp}).}
  \label{fig:expand_vec}
\end{figure}

\begin{figure}[tb]
  \centering
  \includegraphics[width=.85\textwidth]{\dirpath/fig/svg/mpc_eq-expand_uvec}
  \caption{Conversion from cosine of $\varphi(n\Delta t) - \varphi(m\Delta t)$ to the inner product of unit vectors $\mathbf{e}$ in (\ref{eq:vec_mpc}).}
  \label{fig:expand_uvec}
\end{figure}

Highly accurate calculations for the trigonometric function require a significant number of execution cycles implemented as either software or hardware.
With the trigonometric addition theorem and inner product calculations, equations equal to the \ac{mpc} is expanded, and then the proposed method achieves an \ac{mpc} calculation without the trigonometric functions.
%% 
%% explain the following story
%%
For \ac{mpc} from (\ref{eq:eul_mpc}), the equation is formulated and rewritten by the trigonometric addition theorem as follows:
\begin{align}
    \mathrm{MPC} = &\frac{1}{N}\left[ \sum_{i=0}^{N-1}\left(\sin^2[\varphi(i\Delta t)] + \cos^2[\varphi(i\Delta t)]\right) \right. \notag \\
     &+ \left. 2\sum_{n = 1}^{N-1}\sum_{m=0}^{n-1} \Bigl( \sin[\varphi(m\Delta t)]\sin[\varphi(n\Delta t)] + \cos[\varphi(m\Delta t)]\cos[\varphi(n\Delta t)] \Bigr) \right]^{\frac{1}{2}} \notag \\
     = &\frac{1}{N}\left[ N + 2\sum_{n = 1}^{N-1}\sum_{m=0}^{n-1} \cos[\varphi(n\Delta t) - \varphi(m\Delta t)] \right]^{\frac{1}{2}}. \label{eq:f_exp}
\end{align}
  In (\ref{eq:f_exp}), the calculation of the second cosine term can be replaced by the following equation:
\begin{equation}
  \cos[\varphi(n\Delta t) - \varphi(m\Delta t)] = \frac{\mathbf{d}_n \cdot \mathbf{d}_m}{|\mathbf{d}_n||\mathbf{d}_m|},  \label{eq:cos_vec}
\end{equation}
where 
\begin{equation}
  \mathbf{d}_k = (x_{(0,k)}x_{(1,k)} + y_{(0,k)}y_{(1,k)}, y_{(0,k)}x_{(1,k)} - x_{(0,k)}y_{(1,k)}), 
\end{equation}
which is represented in the orthogonal coordinate system and the angular of $\mathbf{d}_k$ is $\varphi(k\Delta t)$ in the polar coordinate system.
% 図の説明
\new{
Here, \figref{fig:expand_vec} explains the conversion from the two input signals $\mathbf{v}_0$ and $\mathbf{v}_1$ to a new vector $\mathbf{d}_k$.
}

Then, it is possible to rewrite (\ref{eq:eul_mpc}) as follows: 
\begin{equation}
\mathrm{MPC} = \frac{1}{N}\left[ N + 2 \sum_{n = 1}^{N-1}\sum_{m=0}^{n-1} \mathbf{e}_n \cdot \mathbf{e}_m \right]^{\frac{1}{2}} \label{eq:vec_mpc}
\end{equation}
\begin{equation}
  \because \frac{\mathbf{d}_n \cdot \mathbf{d}_m}{|\mathbf{d}_n||\mathbf{d}_m|} = \mathbf{e}_n \cdot \mathbf{e}_m.
\end{equation}
\new{
\figureref{fig:expand_uvec} illustrates the conversion from cosine to the inner product of the two vectors $\mathbf{e}$.
One of the advantages to use unit vectors $\mathbf{e}$ is the reduction of calculation amount.
\ac{mpc} calculation with vector $\mathbf{d}$ requires inner product, the norm of vectors, and one division for each combination.
While reuse of calculation results reduces calculation amount, this method needs a large amount of memory to store results.
On the other hand, unit vectors $\mathbf{e}$ allows \ac{mpc} calculation to reduce memory because (\ref{eq:vec_mpc}) requires storing only the unit vectors $\mathbf{e}$.
}

From (\ref{eq:vec_mpc}), the number of the inner products calculations can be reduced as follows: 
\begin{align}
   \sum_{n = 1}^{N-1}\sum_{m=0}^{n-1} \mathbf{e}_n \cdot \mathbf{e}_m
    &= \mathbf{e}_1 \cdot \mathbf{e}_0 + \mathbf{e}_2 \cdot \left( \mathbf{e}_0 + \mathbf{e}_1\right) + \cdots \notag \\
    &+ \mathbf{e}_{N-1} \cdot \left( \mathbf{e}_0 + \cdots + \mathbf{e}_{N-2} \right) \\ 
    &= \sum_{n=1}^{N-1} \left( \mathbf{e}_n \cdot \sum_{i=0}^{n-1} \mathbf{e}_i \right) \label{eq:sigma_uvec} 
\end{align}
Finally, an \ac{mpc} calculation equation is proposed as the following equation, 
\begin{equation}
  \mathrm{MPC} = \frac{1}{N}\left[ N + 2 \sum_{n=1}^{N-1}  \left( \mathbf{e}_n \cdot \sum_{i=0}^{n-1} \mathbf{e}_i \right) \right]^{\frac{1}{2}} \label{eq:formula-prop}.
\end{equation}
The proposed equation and the \ac{mpc} definition equation (\ref{eq:eul_mpc}) are mathematically identical.
Calculation results are coincident with these by (\ref{eq:eul_mpc}) thanks to no approximation.

\new{
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.97\textwidth]{\dirpath/fig/svg/comp-mpc_ref-prop}
  \caption{Comparison of MPC accuracy of proposed method and reference.}
  \label{fig:comp_acc}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.97\textwidth]{\dirpath/fig/svg/comp-mpc_diff-wsize}
  \caption{Comparison of MPC with different window size.}
  \label{fig:comp_dwin}
\end{figure}
}

\new{
For validation of calculation precision, the proposed method was evaluated concerning the calculation accuracy in software simulation compared with a straightforward implementation of (\ref{eq:eul_mpc}).
}
The accuracy is evaluated in MATLAB environment version 2017a, and results are shown in \figref{fig:comp_acc}.
In this evaluation, the \ac{mpc} between two channels is computed with one input held constant at 10 Hz sinusoid, while the other input is swept from 4 Hz to 16 Hz.
The graph shown in \figref{fig:comp_acc} demonstrates that the proposed method has comparable accuracy with reference values calculated by (\ref{eq:eul_mpc}).
This graph demonstrates that the line of the proposed method and that of the reference method is overlapped closely enough, and \ac{rmse} of them is kept less than $1.1 \times 10^{-16}$.
The graph in \figref{fig:comp_dwin} illustrates that the \ac{mpc} value is at unity when the two signals are the same frequency with different window size.
The proposed method cannot express the difference of the frequency of the two input signals with small window size like 10, and the large window size can express the difference in frequency as that of \ac{mpc} value.
% \begin{figure}[tb]
%   \centering
%   \includegraphics[width=.65\textwidth]{\dirpath/fig/svg/mpc_eq-expand_ip-add-reduce}
%   \caption{Reduction of calculation amount in (\ref{eq:sigma_uvec}).}
% \end{figure}

\headline{Comparison of Computational Amount}

\new{
The computational amount of each equation is listed in \tabref{tab:comp_amount}, where $N$ denotes the number of samples in a window.
Regarding normalization of the vectors, multiplying orthogonal coordinate components by \ac{rsqrt} is applied instead of dividing them by \ac{sqrt} because the latency by divider is longer than multiplier in general.
Also, the square root of $X$ is equal to multiplying the $X$ by its \ac{rsqrt} so that the \ac{sqrt} calculator can be eliminated for calculation of (\ref{eq:vec_mpc}) and (\ref{eq:formula-prop}).
Shown in \tabref{tab:comp_amount}, (\ref{eq:vec_mpc}) and (\ref{eq:formula-prop}) can calculate the \ac{mpc} without the trigonometric functions.
Moreover, the computational amount of (\ref{eq:formula-prop}) is proportional to $N$ although (\ref{eq:vec_mpc}) is proportional to the square of $N$.
}

\begin{table}
  \begin{center}
  \caption{Comparison of computational amount}
  \label{tab:comp_amount}
  \input{\dirpath/tab/tab_comp_amount.tex}
  \end{center}
\end{table}

% \section{Evaluation}


% \begin{table}[tb]
%   \begin{center}
%   \caption{Processing Time Comparison}
%   \label{tab:proc_time}
%   \input{\dirpath/tab/tab_proc_time.tex}
%   \end{center}
% \end{table}

% \begin{table}[tb]
%   \begin{center}
%     \caption{Evaluation Environment}
%     \label{tab:sim_env}
%     \input{\dirpath/tab/tab_sim_env.tex}
%   \end{center}
% \end{table}

% In the evaluation of the processing time, the straightforward implementation and the proposed method calculate the \ac{mpc} of two channel signals.
% The input signals include 1,000 samples of 10 Hz and 33 Hz sinusoid, which were divided by the window and computed with different window size; 100, 200, 250, 500, and 1 000 samples per a window. 
% The environmental setting is listed in \tabref{tab:sim_env}.
% Results are listed in \tabref{tab:proc_time}, and they are the median of a thousand average processing time, where each average time is obtained from a thousand trial.
% \tabref{tab:proc_time} shows that the proposed method reduced the processing time of \ac{mpc} by about 20\% compared with the conventional method.
 
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.97\textwidth]{\dirpath/fig/svg/mpc_block_overview_w-eqs}
  \caption{Block diagram of MPC processing element.}
  \label{fig:mpc_pe}
\end{figure}

% \begin{table}[tb]
%   \begin{center}
%   \caption{Number of arithmetic functions}
%   \input{\dirpath/tab/arith_func.tex}
%   \label{tab:c5_arith-func}
%   \end{center}
% \end{table}

\section{Hardware Implementation}

\begin{figure}[tbp]
  \centering
  \begin{minipage}[t]{\textwidth}
    \centering
    \includegraphics[height=.30\textheight]{\dirpath/fig/pdf/input_sin.pdf}
    \subcaption{Input signals; 100 samples of 10 Hz and 8 Hz sinusoid}
    \label{fig:graph_sin}
  \end{minipage}
  \\
  \begin{minipage}[t]{\textwidth}
    \centering
     \includegraphics[height=.30\textheight]{\dirpath/fig/pdf/compare_ex_10-8.pdf}
    \subcaption{Comparison of $ex_i$}
     \label{fig:graph_ex}
  \end{minipage}
  \\
  \begin{minipage}[t]{\textwidth}
    \centering
    \includegraphics[height=.30\textheight]{\dirpath/fig/pdf/compare_accmip_10-8.pdf}
    \subcaption{Comparison of $acc\_ip$}
    \label{fig:graph_accmip}
  \end{minipage}
  \caption{Evaluation of implementation.}
\end{figure}
\begin{figure}
  \ContinuedFloat
  \begin{minipage}[t]{\textwidth}
    \centering
    \includegraphics[height=.30\textheight]{\dirpath/fig/pdf/compare_mpc_10-8.pdf}
    \subcaption{Comparison of $\ac{mpc}$}
    \label{fig:graph_mpc}
  \end{minipage}

  \caption{Evaluation of implementation. (cont.)}
  \label{fig:graph_comp}
\end{figure}

\begin{algorithm}[tb]
  \input{\dirpath/alg/goldschmidt_sqrt}
  \caption{Goldschmidt's \acl{sqrt} and \acl{rsqrt} approximation}
  \label{alg:goldschmidt}
\end{algorithm}

In this section, \ac{rtl} level hardware architecture of the proposed \ac{mpc} calculation method is introduced.
\figureref{fig:mpc_pe} represents a block diagram of the \ac{mpc} processing element, where the black boxes and white boxes indicate registers and functional units, respectively.
The proposed implementation is divided into three stages of calculation: A normalization stage, an inner product stage, and \ac{mpc} calculation stage.
In the proposed implementation, calculation of both \ac{rsqrt} and \ac{sqrt}, which is needed in the normalization stage and \ac{mpc} calculation stage, respectively, adapt Goldschmidt's approach~\cite{Goldshcmid1964} shown in Algorithm~\ref{alg:goldschmidt}.
Compared with the Newton-Raphson's approach, the Goldschmidt's approach changes two calculation modes of \ac{rsqrt} and \ac{sqrt} more simply.
Moreover, its implementation consists of less arithmetic function units.
In the proposed implementation, it takes five cycles to converge the calculation results by the implementation of the Goldschmidt's approach.
The normalization stage uses four adders and three multipliers, the inner product stage uses four adders and one multiplier, the \ac{mpc} calculating stage uses two adders and one multiplier, and the calculator of Goldschmidt's approach uses two adders and four multipliers.
The latency of the implementation is 21 execution cycles: eleven for normalization, three for updating the inner product, and seven for calculating of \ac{mpc}.
The pipeline interval is five cycles because the calculation of either \ac{rsqrt} or \ac{sqrt} requires five cycles.

Comparisons of the reference values and the implementation are shown in \figref{fig:graph_comp}.
\figureref{fig:graph_sin} shows the two input signals for evaluation, where one is 10 Hz sinusoid and the other is 8 Hz sinusoid.
\new{
  Like literature of~\cite{Abdelhalim2010PhaseSync} and~\cite{Romaine2015DAA}, the sinusoid waves are used for the evaluation of accuracy because the feature of input signals is ideal and it is convenient of visualization of experimental results.
}
Figures~\ref{fig:graph_ex} and \ref{fig:graph_accmip} show comparisons between intermediate values of the proposed implementation and those of the reference method.
These graphs show that the proposed implementation has comparable accuracy with the reference method because lines produced by the proposed method match closely to those of the reference method.
\figureref{fig:graph_mpc} depicts the comparison between the \ac{mpc} value of the proposed implementation and those of the reference method.
\new{
  This graph shows that the proposed implementation can calculate the \ac{mpc} with small errors, and the \ac{rmse} is less than 0.15.
}
From results, the proposed method has comparable accuracy for \ac{mpc} calculation compared with the conventional method.
\tableref{tab:compare} summarizes comparison results of simulation and implementation.
\new{
In 16-bit precision, gate count of the proposed implementation was 28389, and the power dissipation was 1.6 mW.
}
% Here, for the fair comparison, the table lists the gate count without \ac{fir} filter.
For evaluation of implementation efficiency, the efficiency is defined by a value that throughput divided by gate count, and then the larger number indicates the high efficiency of hardware implementation.
Therefore, the proposed implementation is about 5.3 times better than Abdelhalim's implementation.
In contrast to Abdelhalim's method using \ac{cordic}~\cite{Abdelhalim2010PhaseSync}, the proposed method can achieve high-performance thanks to the elimination of trigonometric functions.


\begin{table}[tb]
  \begin{center}
    \caption{Comparison of Implementation of Circuit Specification}
    \new{
    \input{\dirpath/tab/tab_compare.tex}
    }
    \label{tab:compare}
  \end{center}
\end{table}

\begin{figure}[tbp]
  \centering
  \begin{minipage}[t]{\textwidth}
    \centering
    \includegraphics[width=.8\textwidth]{\dirpath/fig/pdf/area}
    \subcaption{Area of Circuit.}
    \label{fig:graph_sin}
  \end{minipage}
  \\
  \begin{minipage}[t]{\textwidth}
    \centering
     \includegraphics[width=.8\textwidth]{\dirpath/fig/pdf/n_gates}
    \subcaption{Number of gate counts.}
     \label{fig:graph_ex}
  \end{minipage}
  \\
  \begin{minipage}[t]{\textwidth}
    \centering
    \includegraphics[width=.8\textwidth]{\dirpath/fig/pdf/power}
    \subcaption{Power dissipation.}
    \label{fig:graph_accmip}
  \end{minipage}
  \caption{Relevance of bit width in proposed circuit and results of logical synthesis.}
  \label{fig:graph_precision}
\end{figure}

\section{Discussion}
From the evaluation results, this section discusses the hardware implementation efficiency of the proposed method.
Have mentioned above, the closed-loop stimuli controls require a simultaneous pursuit of high-speed performance and low hardware cost for its signal processing.
The proposed method can calculate the \ac{mpc} with high throughput and low hardware cost, which is demonstrated with the metric of ``throughput per gate count.''

The proposed implementation can calculate the \ac{mpc} with only addition, multiplication, \ac{rsqrt}, and \ac{sqrt}, and it needs 21 execution cycles per one execution.
In Abdelhalim's work~\cite{Abdelhalim2010PhaseSync}, \ac{cordic} is utilized for calculation of trigonometric function and root square, and it needs 54 execution cycles per one execution.
The \ac{cordic} can calculate trigonometric functions and \ac{sqrt} with enough accuracy, however, it needs a large number of execution cycles until completing calculation.
In contrast, the proposed method replaces the trigonometric functions with inner product calculation, which requires additions, multiplications, and \ac{rsqrt}. 
The addition and multiplication can be completed in only one cycle.
The \ac{rsqrt} is calculated by Goldschmidt's approximation~\cite{Goldshcmid1964}, which can calculate its result with enough accuracy in smaller execution cycles than the \ac{cordic}.
Therefore, the proposed implementation increases the throughput of \ac{mpc} calculation by 263~\% compared with the Abdelhalim's work~\cite{Abdelhalim2010PhaseSync}.
This improvement will allow the implantable neural analyzer to miniaturize its volume and accelerate its processing time, and then these will lead more promotion to neuroscience study and clinical use.
 The proposed implementation also achieves higher hardware implementation efficiency than the Abdelharlim's work.
Note that the hardware implementation efficiency defined above does not depend on their operating frequency or technology process.
The proposed method increases the hardware implementation efficiency by 529~\% compared with the conventional implementation.

\new{
% グラフの話
% 面積、ゲート数、電力
The bars in \figref{fig:graph_precision} depict the relevance of bit width in the proposed implementation and characteristics.
In the Abdelhalim's work~\cite{Abdelhalim2010PhaseSync}, the signal processor has 10-bit \ac{mpc} and 16-bit input signals are used.
Each graph demonstrates the characteristics from 10-bit to 16-bit precision.
These figures show that the proposed implementation in 10-bit precision was 26860 $\mu m^2$, 14303 gates, and 0.8 mW power dissipation.
Hence, the hardware implementation efficiency is 10.8 times better with the same output precision compared with the Abdelhalim's work~\cite{Abdelhalim2010PhaseSync}.
Mentioned above, this thesis applies the implementation in 16-bit precision according to the bit width of the input signals.
}
 
% For evaluation from another point of energy efficiency, energy consumption $E$ is roughly given by the following equation: 
% \begin{equation}
%   E = \frac{cP}{f}, 
% \end{equation}
% where $c$, $P$, and $f$ represent the number of execution cycles required for the \ac{mpc} calculation, the power consumption of the implementation, and the operation frequency, respectively.
% The execution cycles $c$ can be calculated as follows: 
% \begin{equation}
%   c = \frac{S}{T},
% \end{equation}
% where $S$ and $T$ denote the window size of the MPC calculation and throughput of the implementation, respectively.
% Hence, the comparison of energy efficiency $r$ between that of the proposed method and the conventional method under the same operation frequency and window size, they can be calculated as follows: 
% \begin{equation}
%   r = \frac{E_p}{E_c} = \frac{P_p S_p}{P_c S_c} \frac{T_c}{T_p}, 
% \end{equation}
% where subscripts $p$ and $c$ represent the values produced by the proposed implementation and the conventional implementation, respectively.
% Therefore, the $r$ of the proposed implementation and Abdelhalim's work is 0.88, which means the proposed method increases energy efficiency by 12\%  roughly compared with Abdelhalim's work.


%
%More the number of recording channel increase,  more the accurcy of MPC affects.



\section{Conclusion}
In this chapter, a hardware-oriented \ac{mpc} calculation method was proposed.
For improvement of stimulation control, the biomedical signal processing has been required a simultaneous pursuit of high-speed performance and low-cost implementation.
\ac{mpc} calculation is one of the well-known methods for feature extraction of biomedical signals, and it has been utilized for detection and prediction of the epilepsy seizure.
The proposed method attains high computational efficiency by substituting trigonometric functions with the computation of linear algebra.
In the comparison between software implementations, the proposed method achieved about 20~\% reduction
in processing time compared with the straightforward implementation of definition equation of \ac{mpc}.
This chapter also proposed a hardware implementation that aimed at a simultaneous pursuit of high throughput processing and implementation efficiency.
From evaluation results, the proposed implementation improved throughput of the MPC calculation by 2.6 times and the hardware implementation efficiency by 5.3 times, respectively, compared with the conventional implementation.
\new{
  This improvement in hardware implementation will provide the neural analysis devices with more miniaturization  and processing power.
}
