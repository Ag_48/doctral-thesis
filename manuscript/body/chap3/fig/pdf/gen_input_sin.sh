#!/bin/bash -u
in=input_sin_idx
out=input_sin.pdf
gnuplot <<- EOF
  set terminal pdfcairo
  set output '${out}'
  set xlabel "index"
  set ylabel "amplitude"
  set mxtics 1.0
  plot '${in}' using 1:2 title '10 Hz sinusoid' with lp pt 1 lc rgb "cyan", '${in}' using 1:3 title '8 Hz sinusoid' with lp pt 4 lc rgb "magenta"
  quit
EOF
