\renewcommand{\dirpath}{body/chap5}

\chapter{Low Energy Architecture of Neural Stimulation Controller}

Chapter 4 discussed a low computational data compression method to reduce energy consumption in the wireless communication system for the neural prosthesis systems and its implementation as an \acf{asip}.
This chapter describes a design of neural stimulator architecture including the \ac{asip} as a stimulation controller.
The neural stimulator is designed as a \acf{soc}.
First, requirements for neural stimulation devices to realize practical sensory perception are explained, and this thesis focuses on how to stimulate the target nerves "strictly" by a dedicated hardware and proposes the design of a stimulation controller.
Finally, an \ac{soc} architecture for a controller of the internal body component, which includes the proposed \ac{asip} as a microprocessor in chapter 4, is proposed and evaluated.

\section{Motivation and Objective}

\begin{figure}[tb]
  \centering
  \includegraphics[width=.9\textwidth]{\dirpath/fig/obj/task_timeline}
  \caption{Task management of internal body microcontroller.}
  \label{fig:c5_tl}
\end{figure}

% ここにStimulatorとその他の関係の図がほしい

% Low-energyの話はいつするのか？

Electrical neuromodulation treatments have been expected to be effective for drug-resistant neural diseases such as epilepsy, and some neural stimulation devices are available on the market.
Approximately 30\% of epileptic patients remain either drug-resistant or remain limited adverse effect~\cite{kwan2011drug}.
Although the neural stimulation devices have possibilities for the contribution of medicine, many challenges remain toward improved safety and \acf{qol}.
Regarding both clinical and research use, the neural stimulation devices require the followings: Flexibility for stimulation strategies, microseconds order temporal resolution, and low energy design.

Deciding appropriate parameters of stimulation, such as strength, duration, phase, and timing, before using neural stimulation devices is extremely difficult because their proper values depend on individual difference.
For fixing unexpected behavior and avoiding undesirable effects of stimulation, neural stimulation devices require flexibility in stimulation in both fields of experimental and clinical.
To satisfy this requirement, a possible solution is that a component for generating stimulation separates from that for controlling stimulation in architecture level.
For ensuring high flexibility of stimulation strategies, the stimulation controller includes more than or equal to one processor, which can adopt a new stimulation strategy by loading new data to referring memories.
% GPPはその場で計算できるので，closed-loop として適切な（収束するように）パラメータを決定できるようになるだろう
The processors can perform calculations \textit{in situ}; they may calculate the stimulation parameters by using values from peripheral sensors as closed-loop if there are appropriate control algorithms.

The second requirement, microseconds order for stimulation, is relatively complicated to satisfy.
Owe to the notable improvement of the embedded systems, commercial microcontrollers designed for low energy operation has enough performance to control stimulation in the microseconds order if they are devoted to only stimulation control.
However, there are many tasks to manage the internal body component as depicted in \figref{fig:c5_tl}.
As shown in this figure, what the microcontrollers have to deal with is classified into two types: Periodic tasks and aperiodic tasks.
The periodic tasks contain power management% of inductive coupling
, thermal monitoring caused by power dissipation of the device, logging of system status, and so on.
The microcontrollers commonly process these task by interrupts from timers in the \ac{soc}, which period is programmed in advance.
In contrast, the aperiodic tasks including the stimulation task contained communication from the external \ac{soc} modules via wireless such as Bluetooth and \acf{uwb} and wired such as \acf{spi} and \acf{uart}.
The difficulty of the stimulation task management is the requirements for high temporal resolution and precision.
In general, the processors carry out instructions in the instruction memory sequentially.
However, they cannot process strictly in time because of pipeline-hazards and external or internal interrupt by peripheral modules such as timers.
The stimulation tasks occupy the processing of the microcontroller for a long time to satisfy these requirements, and hence the microcontroller cannot process the other tasks.
For ensuring high temporal resolution and precision, the stimulation controller needs to include a circuit dedicated to controlling stimulation strictly in microseconds resolution, which is processing stimulus in parallel the processors. 

A space for implanting the neural stimulation devices and the amount of wireless power supply are limited.
Low energy design of them enables to reduce the volume of a secondary battery and extend their operation time.
The low energy design also expands the longevity of the systems, which can increase their \ac{qol} by reducing the number of additional surgeries for device replacement.
% In the neural stimulation devices, the stimulation by electrodes consumes the most of all energy.
To reduce the energy consumption, this thesis applies the data compression method discussed in chapter 4, which can reduce the stimuli position data for the visual prosthesis by 77\% in average.
\new{
In the visual prostheses, the neural stimulation devices require both high-frequency and high-configurable stimulation devices because the visual prostheses devices have to complement the gap between the ideal visual field in the external image processor and reported visual recognition by the user.
Therefore, the proposed \ac{soc} will contribute to the construction of practically visual recognition for the visual prosthesis devices.
}


% low energy はここらへんで話す必要がある．ということは前に話さなければ．

This study proposes a high-flexibility, high temporal resolution, and low energy architecture of neural stimulator.
The \ac{asip} allows the proposed \ac{soc} to change stimulation strategies with only rewriting data in memories, and the dedicated control signal generator enables the proposed \ac{soc} to stimulate the target nerves in microseconds order in parallel to the other tasks processed by the processors.

%
% According to BioCAS2016
%

\section{Architecture Overview}

\begin{figure}[tb]
  \centering
  \includegraphics[width=0.97\linewidth]{\dirpath/fig/svg/overview-int-body-component}
  \caption{Overview of the target neural stimulation device and proposed \acs{soc}.}
  \label{fig:c5_overview-neurostim}
\end{figure}

% Explanation for Stimulator Digital part
%
% 
%

% なぜこのデザインなのか?
In this section, an overview of the proposed \ac{soc} architecture is introduced.
The proposed \ac{soc} is designed for flexibility regarding stimulation strategies, and then it applies common-used protocols for communication other modules in the internal body component.
Also, the proposed \ac{soc} has an \acf{ocp} connected to a discrete flash memory, which is convenient for replacement of \ac{asip} program.

\new{
\figureref{fig:c5_overview-neurostim} illustrates an overview of a target neural stimulation device that includes the proposed \ac{soc}.
The target neural stimulation device consists of an external body component and an internal body component.
The two components communicate via wireless communication for neural stimulation and recorded data.
This study focuses on the internal body component, and it consists of a wireless communication module, the proposed \ac{soc}, and a signal converter for stimuli generator.
The proposed \ac{soc} is connected to the signal converter and the wireless communication module on the board, and the signal converter is connected to an electrodes array for stimulation.
The electrodes array penetrated into the target nerve, which is the visual cortex in this study, and it has more than hundreds of electrodes to realize the desired phenomenon in high-spatial resolution.
The signal converter was presented in \cite{Hayashida2015_CorticalNeuralExcitations} by Hayashida \etal as a prototype composed by discrete components and designed by Kameda (ref.~\cite{Kameda2014_StimulatorChip}), which has high flexibility for stimulation parameters such as stimuli position (64 channels), amplitude (7-bit precision: $\pm$ 100, $\pm$ 200, $\pm$ 300, and $\pm$ 400 $\mu$ A/phase), and so on.
To bring out its performance, the proposed \ac{soc} includes the stimulation controller to manipulate the stimuli in microsecond order.
Here, the combination of the proposed \ac{soc} and the signal converter is evaluated in this thesis.
}

\begin{figure}[tb]
  \centering
  \includegraphics[width=0.97\linewidth]{\dirpath/fig/obj/soc_if_overview}
  \caption{Block diagram of proposed SoC interface.}
  \label{fig:c5_soc-if}
\end{figure}

\headline{\acs{soc} Interface}
For the convenience of development, the internal stimulation controller has to have two types of communication interfaces for existing components: Inter-device and inter-chip.
\figureref{fig:c5_soc-if} illustrates the interface between the proposed \ac{soc} architecture and peripheral components.
The proposed \ac{soc} adopts \ac{uart} for the inter-device communication.
\ac{uart} is easy to implement and commonly used for asynchronous communication with a computer.
The computer can read and write internal the \ac{soc} status for debugging use via \ac{uart}.
For the interaction with the external body component, the proposed \ac{soc} applies discrete wireless communication \ac{soc} and communicates with it via the inter-chip communication.

In this thesis, the inter-chip communication is classified into two types: For the discrete chips and the signal converter.
The former is used for the communication between the proposed \ac{soc} and the discrete \ac{soc} for peripheral modules including the wireless communication and sensors.
The communication for the discrete chips require usability, and then the proposed \ac{soc} adopts \ac{spi} protocol, which is commonly used for inter-chip communication.
The \ac{spi} protocol enables the proposed \ac{soc} to communicate with plural modules through a single interface, which indicates that the proposed \ac{soc} has expandability for the whole design of peripheral modules.
% Also, communication between the \ac{ocp} and \ac{

The communication with the signal converter is divided into two parts: data transmission and control stimulation.
To configure amplitude and position of stimulation, the proposed \ac{soc} communicates with the signal converter via \ac{spi} protocol.
Because control of stimulation requires high temporal resolution, the proposed \ac{soc} has output ports for individual signals for the input port of the signal converter, and the detail is explained after.

\begin{figure}[tbp]
  \centering
  \includegraphics[width=0.9\linewidth]{\dirpath/fig/svg/soc_overview}
  \caption{Block diagram of inside body component.}
  \label{fig:c5_soc}
\end{figure}

\headline{Components in \acs{soc}}
\figref{fig:c5_soc} depicts the structure of the stimulation controller.
% SoC と外部モジュールとの通信の詳細について記載すること
The proposed stimulation controller consists of the following components: an on-chip programmer, an external SPI flash memory, a 16-bit microprocessor, an instruction memory, a data memory, and a control signal generator for the signal converter.
The microprocessor connects the instruction memory directly, and the data memory, the internal wireless communication unit, the control signal generator are connected via a system bus.
In this study, the \ac{asip} proposed in chapter 3 is utilized as a microprocessor  of the \ac{soc}, and, for the convenience of reconfiguration, the flash SPI memory stores the compiled binary data.

Then, the following sentences explain how to perform the stimulation controller from reset to generating stimulation.
When the inside module is reset, the on-chip programmer loads the compiled binary data in the external flash SPI memory and stores them to the instruction memory and the data memory.
After storing data to these memories, the on-chip programmer sends the reset signal to the microprocessor, and then the microprocessor starts initialization of peripheral modules.
When the internal communication unit receives the stimulation information, the microprocessor stores the stimulation data for the control signal generator in registers. 

\begin{figure}[tbp]
  \centering
  \includegraphics[width=0.9\linewidth]{\dirpath/fig/obj/control_signal_generator}
  \caption{Control signal generator.}
  \label{fig:c5_csg}
\end{figure}

\headline{Control Signal Generator}
\figurename~\ref{fig:c5_csg} illustrates the details of the control signal generator and its interface.
For simplicity of programming, the microprocessor controls the stimulation by sending several kinds of commands to the control signal generator, which generates control signals for the signal converter independently from the microprocessor.

\begin{figure}[tbp]
  \begin{minipage}[t]{\textwidth}
    \centering
    \includegraphics[width=\textwidth]{\dirpath/fig/obj/time_chart_stimlation.pdf}
    \subcaption{Eight sets of microstimulation and non-stimulation interval time}
    \label{fig:tc_stim}
  \end{minipage} 
  \\
  \begin{minipage}[t]{\textwidth}
    \centering
    \includegraphics[width=\textwidth]{\dirpath/fig/obj/time_chart_control_signals.pdf}
    \subcaption{One sequence of microstimulation including anodic primary stimulation and cathodic secondary stimulation.}
    \label{fig:tc_cntr_sigs}
  \end{minipage}
  \caption{Timing chart of control signals for the signal converter.}
\end{figure}

\begin{table}[tb]
  \begin{center}
    \caption{Control signals for signal converter}
    \input{\dirpath/tab/tab_cntsig_sigconv.tex}
    \label{tab:cntsig_sigconv}
  \end{center}
  % \vspace{-2em}
\end{table}

The following describes details of each control signal listed in \tablename~\ref{tab:cntsig_sigconv}.
When signals TSI and CST are set to high, the signal converter stimulates the target nerve referring to the configuration values in registers in the signal converter, and it stops stimulating when either signal is set to low.
When signal POL is set to low or high, the polarity of stimulation is set to cathodic or anodic, respectively.
In standard physiological experiments, a biphasic current pulse stimulation is used to avoid charge accumulation in tissue, so that the signal converter is designed to generate a microstimulation that consists of a pair of anodic and cathodic stimulation.
When the signal C2R is high, the output of the signal converter connects to the ground.
The signal DCLK has two roles: as a state transition signal for the signal converter and as an adjuster for the duration of secondary stimulation that is utilized to release the electric charge from the tissue because of the first stimulation.
The stimulation has to have a non-stimulation time to avoid inflammation. 
When the signal NST is high, the signal converter stops generating stimulation.
\figurename~\ref{fig:tc_stim} shows a timing chart of an example of stimulation train, whose repetition of anodic-first microstimulation is eight.

A set of stimulation is divided into two sections: sequences of microstimulation and non-stimulation time, and a sequence of microstimulation consists of three parts: primary stimulation, secondary stimulation, and an interval of stimulation.
\figurename~\ref{fig:tc_cntr_sigs} depicts a timing chart for the output signals from the stimulation generator.
In this figure, sections of B, D, and G indicate the primary stimulation, the secondary stimulation, and the interval of stimulation, respectively.
To start or repeat the microstimulations, two pulses are sent via DCLK.
To stop the microstimulation, sixteen pulses are sent via DCLK, whose rising edge of the first pulse need to synchronize falling edge of CST to adjust the duration of secondary stimulation in the signal converter.
One pulse of CST needs to be anodic, and the other needs to be cathodic to realize biphasic stimulation.
\tablename~\ref{tab:spec} summarizes specifications of the proposed stimulation controller.

The signal converter consists of the registers for stimulation parameters, and the control signal generator configures them via the serial communication before or during stimulation.
After a start of stimulation, the control signal generator runs continuously even while setting the stimulation parameters by the microprocessor.
The signal xRST is used to reset signal of the signal converter.
The signals SCK and SDI are used for serial communication to the signal converter, and the signal xCS and xRS are used as selective signals.
 % The signals correspond to the register in the signal converter, and they are isolated each other.
When the xCS is low, serial bit data on the SDI indicates an index of the electrode array.
When one of the signals in the xRS is low, the data on the SDI indicate the parameters for the microstimulation of the electrode array directed by the index.

\begin{table}
  \begin{center}
  \caption{Specification of proposed stimulation controller}
  \label{tab:spec}
  \input{\dirpath/tab/tab_exp_def_setting.tex}
  \end{center}
  \vspace{-2em}
\end{table}

% ここにプロセッサとの刺激発現フローの説明(w/ time-chart)


\section{Evaluation}
This section explains evaluation experiments with an \ac{fpga} to validate the output wave signal of the proposed stimulation controller, and it also describes a simulation result for the proposed \ac{soc} architecture.
The proposed stimulation controller was implemented using Quartus Prime by Altera Corp., and the target board was Altera Cyclone IV E in DE2-115 Development and Education Board.
In the experiments, the logic analyzer Logic Pro 16 by Saleae, Inc. was used.
Design Compiler from Synopsys, Inc. with a GLOBALFOUNDRIES 0.18$\mu$m CMOS library was used to estimate the area and power dissipation.


\subsection{FPGA Implementation}
\begin{figure}[tb]
    \centering
    \includegraphics[width=0.98\textwidth]{\dirpath/fig/jpg/pict_fpga_gs}
    \caption{Photograph of the experimental setup for functional validation with FPGA.}
    \label{fig:experiment_view}
\end{figure}

\begin{figure}[tbp]
  \begin{minipage}[t]{\textwidth}
    \centering
      \includegraphics[height=0.6\textheight]{\dirpath/fig/svg/serial+stim}
      \subcaption{Control signals in setting position of stimulation and eight sets of microstimulation}
      \label{fig:serial+stim}
  \end{minipage}
    \\
  \begin{minipage}[t]{\textwidth}
    \centering
      \includegraphics[width=\textwidth]{\dirpath/fig/svg/stim_default_single.pdf}
      \subcaption{One sequence of microstimulation.}
      \label{fig:wave_fpga_single}
  \end{minipage}
  \caption{Waveforms recorded by logic analyzer from FPGA.}
  \label{fig:wave_fpga_stim}
\end{figure}

\begin{figure}[tbp]
\begin{center}
  \begin{minipage}[t]{\textwidth}
    \centering
      % \includegraphics[width=\textwidth]{\dirpath/fig/svg/serial_reg3_part.pdf}
      \includegraphics[height=0.3\textheight]{\dirpath/fig/svg/serial_reg3_part.pdf}
      \subcaption{Setting amplitude}
      \label{fig:serial_reg3}
  \end{minipage}
    \\
  \begin{minipage}[t]{\textwidth}
    \centering
      \includegraphics[height=0.3\textheight] {\dirpath/fig/svg/serial_reg2_part.pdf}
      \subcaption{Setting order of stimulation}
        \label{fig:serial_reg2}
  \end{minipage}
  \\
  \begin{minipage}[t]{\textwidth}
    \centering
    \includegraphics[height=0.3\textheight]{\dirpath/fig/svg/serial_reg1_part.pdf}
      \subcaption{Setting position of stimulation}
    \label{fig:serial_reg1}
  \end{minipage}
  \caption{Waveform recorded by logic analyzer at the start of setting registers.}
  \label{fig:wave_fpga_regs}
  \end{center}
\end{figure}

\begin{figure}[tbp]
  \begin{minipage}[t]{\textwidth}
    \centering
      \includegraphics[width=\textwidth]{\dirpath/fig/svg/stim_rep6.pdf}
      \subcaption{Changing repetition of microstimulation to six-time}
      \label{fig:wave_fpga_rep6}
  \end{minipage}
    \\
  \begin{minipage}[t]{\textwidth}
    \centering
      \includegraphics[width=\textwidth]{\dirpath/fig/svg/stim_pol_rev_single.pdf}
      \subcaption{Changing polarity of stimulation}
      \label{fig:wave_fpga_pol_rev}
  \end{minipage}
  \\
  \begin{minipage}[t]{\textwidth}
    \centering
      \includegraphics[width=\textwidth]{\dirpath/fig/svg/stim_len1x2_single.pdf}
      \subcaption{Changing duration of primary stimulation to 400 $\mu$s}
      \label{fig:wave_fpga_len1x2}
  \end{minipage}
  \caption{Waveforms modified configuration from \figref{fig:wave_fpga_single}.}
  \label{fig:wave_fpga_mod}
\end{figure}

\begin{table}[tb]
  \begin{center}
  \caption{Results of Compilation for \acs{fpga}}
  \label{tab:res_comp}
  \input{\dirpath/tab/tab_result_comp.tex}
  \end{center}
\end{table}

\begin{table}[tb]
  \begin{center}
  \caption{Results of Compilation for \acs{soc}}
  \label{tab:res_soc}
  \input{\dirpath/tab/result_soc.tex}
  \end{center}
\end{table}



\figurename~\ref{fig:experiment_view} depicts the setup of the experimentations.
An \ac{spi} flash memory was connected to the \ac{fpga}, and the logic analyzer connected with the GPIO of the \ac{fpga}.
In this evaluation, internal \ac{soc} status was logged by a desktop computer via \ac{uart} protocol.
Stimulation parameters were set and changed by the replacement of program in the \ac{spi} flash memory.

The compilation results for the FPGA are summarized in \tablename~\ref{tab:res_comp}.
% 写真に関する説明を挿入.
Figures~\ref{fig:serial+stim} and \subref{fig:wave_fpga_single} show waveforms recorded by the logic analyzer on signals of the FPGA, and \figref{fig:serial+stim} indicates that the FPGA repeated stimulation in eight times as setting.
\figureref{fig:wave_fpga_regs} shows waveforms for sending the pair of data composed of the index of electrode arrays and the settings of stimulation to each register in the signal converter as specified.
Figures~\ref{fig:serial_reg3}, \subref{fig:serial_reg2}, and \subref{fig:serial_reg1} illustrate the waveforms of transmission that sends the setting of amplitude, order, and position of the stimulation, respectively.

Next, experiments changed the stimulation parameters: Repetition, polarity, and duration.
\figref{fig:wave_fpga_rep6} shows that a modification that the number of microstimulations is set from eight, which is depicted in \figref{fig:serial+stim}, to six was reflected correctly.
% The waveform from the stimulation controller that microstimulation repetition was changed to six is shown in \figref{fig:wave_fpga_rep6}.
Figures~\ref{fig:wave_fpga_pol_rev} and \subref{fig:wave_fpga_len1x2} also show the modifications were reflected correctly in polarity, and duration of stimulation, respectively.
These results demonstrate that the proposed stimulation controller has high flexibility for the stimulation parameters and the stimulation strategies.

\subsection{SoC Evaluation}
\begin{figure}[tb]
  \centering
  \includegraphics[width=\textwidth]{\dirpath/fig/png/soc_layout_rotate_gs}
  \caption{SoC layout design.}
  \label{fig:c5_soc-layout}
\end{figure}

\begin{table}[tb]
  \begin{center}
    \new{
    \caption{Specification of proposed neural stimulation device set}
    \input{\dirpath/tab/soc_spec.tex}
    \label{tab:c5_soc-spec}
    }
  \end{center}
\end{table}

For evaluating the performance in case that the proposed controller is implemented as an \ac{soc}, this thesis carried out logical synthesis and designed \ac{soc} layout. 
The logical synthesis results are summarized in of \tablename~\ref{tab:res_soc}.
The design results show that the proposed control signal generator occupies a larger area and consumes an enormous amount of power than the \ac{asip}.

%From this result, the proposed \ac{soc} obtains high flexibility for the stimuli configuration in exchange for twice power dissipation compared with only that of only control signal generator.
Also, \figureref{fig:c5_soc-layout} depicts a \ac{soc} layout screenshot from a placement and routing tool.
Due to a large number of IO cells, die size of the proposed \ac{soc} was 2.1 cm $\times$ 3.0 cm.
Finally, \tableref{tab:c5_soc-spec} summarizes specifications of the proposed \ac{soc}, which includes the specifications of the signal converter proposed in~\cite{Kameda2014_StimulatorChip}.
From these results, the proposed \ac{soc} achieved a simultaneous pursuit of high temporal resolution and high flexibility by the combination of an \ac{asip} and a circuit dedicated to stimulation.
%
% SoCの評価用テーブルが必要，前のセクションと切り離す
% 


\section{Discussion}

\begin{table}[tb]
  \begin{center}
  \new{
    \caption{Comparison with the conventional neural stimulator}
    \input{\dirpath/tab/comp_neurostim.tex}
    \label{tab:c5_comp-neurostim}
  }
  \end{center}
\end{table}
\new{
This section discusses the flexibility of the proposed device for stimuli parameter by comparing other studies.
The evaluation results show that the proposed device has the microsecond-order temporal resolution (up to 2 MHz) and 64-channel stimulation sites.
The comparison of characteristics of stimuli parameter is listed in \tabref{tab:c5_comp-neurostim}.
The number of the stimuli channels and the stimuli amplitude resolution are comparable to the state-of-the-art architectures.
The flexibility of the stimuli frequency and stimuli duration, also known as pulse width range, is better than that of the other studies.
Regarding power consumption of digital core of the proposed device exceeds that of other studies due to the adoption of the microprocessor.
}

\new{
In this thesis, the proposed neural stimulation device includes the stimulation controller and the microprocessor.
Owe to the stimulation controller, the proposed device has enough performance to realize a simultaneous pursuit of high-temporal resolution and high-flexible stimulation.
This will result in sufficient visual recognition by the visual prosthesis devices.
Regarding closed-loop adjustment of stimuli parameters, the microprocessor will play an important role in future to calculate accommodate parameters \textit{in situ} in the skull.
This contributes not only to neuroscience and electrophysiology as an experimental prototype of neural stimulation device but also to neural stimulator in clinical use.
Concern about heat dissipation by \ac{imd}~\cite{SKim2007ThermalImpact} and energy consumption by stimulation~\cite{Little2013, HWu2015} will be a big issue in clinical scene.
Hence, the microprocessor will be helpful to manage heat and energy in the neural stimulation devices to scavenge kinds of information from sensors and calculate accommodated stimuli parameters for the individual brain.
Also, the external memory connected with the proposed \ac{soc} makes easy to change stimulation and calibration methodologies by rewriting programs.
Therefore, the research results of the proposed device will lead to better \ac{qol} to the users of the neural stimulation devices.
}

\section{Conclusion}

This chapter proposed highly flexible and spatial stimulation controller.
For high flexibility to stimulation strategies, this chapter applied the ASIP proposed in chapter 4 as the stimulation controller.
Processors including \ac{asip} cannot guarantee microseconds order temporal resolution because of external interrupts from peripheral modules, and hence this chapter designed the dedicated circuit to control stimulation in high temporal resolution.
Experimental results showed that the proposed \ac{soc} architecture could control stimulations in microseconds order with \ac{fpga} implementation. 
\new{
The proposed device has comparable potentials with the state-of-the-art neural stimulation devices, and it will contribute to the alleviation of concerns about heat dissipation and energy consumption by adjusting the stimuli spatio-temporal parameters for the variation of the individual brain \textit{in situ}.
}

