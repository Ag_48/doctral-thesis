set xl "Run length"
set yl "Frequecy of occurrence [%]"
set xrange [1:1024]
set yrange [0:50]
set key right top
set size 0.5,0.5



set logscale x 2
plot "hist_raw_all.dat" using 1:2 w lp pt 2 title "raw_all"
set terminal postscript eps
set output "graph_raw_all.eps"
replot

set terminal x11
plot "hist_raw_diff.dat" using 1:2 w lp pt 2 title "diff_all"
set terminal postscript eps
set output "graph_diff_all.eps"
replot


set terminal x11
plot "hist_raw_0.dat" using 1:2 w lp pt 1 title "raw_0"
replot "hist_raw_1.dat" using 1:2 w lp pt 6 title "raw_1"
set terminal postscript eps
set output "graph_raw_01.eps"
replot

set terminal x11
set yrange[0:70]
plot "hist_diff_0.dat" using 1:2 w lp pt 1 title "diff_0"
replot "hist_diff_1.dat" using 1:2 w lp pt 6 title "diff_1"
set terminal postscript eps
set output "graph_diff_01.eps"
replot
